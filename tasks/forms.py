from django.forms import ModelForm, DateTimeInput, DateInput
from tasks.models import Task, TaskComment


# DateTime Picker Widget
class DateTimeInputWidget(DateTimeInput):
    input_type = "datetime-local"


# Date Picker Widget
class DateInputWidget(DateInput):
    input_type = "date"


# Form to Create a Task
class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
        widgets = {
            "start_date": DateInputWidget(),
            "due_date": DateTimeInputWidget(),
        }


# Form to Create a Task Comment
class TaskCommentForm(ModelForm):
    class Meta:
        model = TaskComment
        fields = [
            "comment",
        ]
