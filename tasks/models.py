from django.db import models
from django.conf import settings
from django.utils import timezone
from projects.models import Project


# Model for Tasks
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name

    @property
    def is_past_due(self):
        return timezone.now() > self.due_date

    @property
    def hours_until_due(self):
        time_delta = self.due_date - timezone.now()
        hours_until_due = time_delta.days * 24 + time_delta.seconds / 3600
        return hours_until_due
        # Deprecated back-end logic, now performed on front-end:
            # if self.is_completed:
            #     return "completed"
            # elif hours_until_due < 0:
            #     return "overdue"
            # elif hours_until_due < 24:
            #     return "at risk"
            # else:
            #     return "on track"



# Model for Comments on Tasks
class TaskComment(models.Model):
    comment = models.TextField()
    date = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="task_comments",
        on_delete=models.PROTECT
    )
    task = models.ForeignKey(
        Task,
        related_name="task_comments",
        on_delete=models.CASCADE,
    )
    parent_comment = models.ForeignKey(
        "self",
        related_name="replies",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
