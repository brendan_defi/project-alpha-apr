from django.contrib import admin
from tasks.models import Task, TaskComment


# Admin Model for Task
@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "project",
    ]


# Admin Model for Comment
@admin.register(TaskComment)
class TaskCommentAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "task",
        "author",
    ]
