from django.urls import path
from tasks.views import create_task, show_my_tasks, update_task, \
    toggle_task_completion, show_task_detail, create_task_comment, \
    create_task_comment_reply



urlpatterns = [
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
    path("<int:task_id>/", show_task_detail, name="show_task_detail"),
    path("<int:task_id>/edit/", update_task, name="update_task"),
    path("<int:task_id>/toggle_completion/", toggle_task_completion, name="toggle_task_completion"),
    path("<int:task_id>/comment/", create_task_comment, name="create_task_comment"),
    path("<int:task_id>/comment/<int:task_comment_id>/reply/", create_task_comment_reply, name="create_task_comment_reply"),
]
