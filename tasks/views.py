from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from tasks.models import Task, TaskComment
from tasks.forms import TaskForm, TaskCommentForm
from common.helper_functions import get_project_membership_level
from common.decorators import permissmions_required
from projects.models import ProjectMembership


# Create a New Task
@login_required
def create_task(request):
    if request.method == "GET":
        request.session["prev_page"] = request.META.get('HTTP_REFERER')

    form = TaskForm(request.POST) if request.method == "POST" else TaskForm()

    if request.method == "POST" and form.is_valid():
        assignee = form.cleaned_data["assignee"]
        project = form.cleaned_data["project"]
        assignee_project_membership_level = get_project_membership_level(assignee, project.id)
        user_project_membership_level = get_project_membership_level(request.user, project.id)

        if user_project_membership_level == "Owner" or user_project_membership_level == "Edit":

            if assignee_project_membership_level == "Owner" or assignee_project_membership_level == "Edit":
                form.save()
                return redirect(request.session.get('prev_page', '/'))

            else:
                form.add_error(None, "The specified assignee is not an owner or editor of the specified project.")

        else:
            form.add_error(None, "You are not allowed to create tasks for this project.")

    context = {
        "form": form,
    }

    return render(request, "tasks/create_task.html", context)


# Show User Tasks
@login_required
def show_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user).order_by("is_completed")

    context = {
        "my_tasks": my_tasks,
    }

    return render(request, "tasks/show_my_tasks.html", context)


# Edit a Task
@login_required
@permissmions_required("Owner", "Edit")
def update_task(request, task_id):
    if request.method == "GET":
        request.session["prev_page"] = request.META.get('HTTP_REFERER')

    task = get_object_or_404(Task, id=task_id)
    form = TaskForm(request.POST, instance=task) if request.method == "POST" else TaskForm(instance=task)

    if request.method == "POST" and form.is_valid():
        assignee = form.cleaned_data["assignee"]
        project = form.cleaned_data["project"]
        assignee_project_membership_level = get_project_membership_level(assignee, project.id)

        if assignee_project_membership_level == "Owner" or assignee_project_membership_level == "Edit":
            form.save()
            return redirect(request.session.get('prev_page', '/'))

        else:
            form.add_error(None, "The specified assignee is not an owner or editor of the specified project.")

    context = {
        "form": form,
    }

    return render(request, "tasks/update_task.html", context)


# Complete/Reopen a Task
@login_required
@permissmions_required("Owner", "Edit")
def toggle_task_completion(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    task.is_completed = not task.is_completed
    task.save()

    return redirect(request.META.get('HTTP_REFERER'))


# Show Task Detail w/ User Access Gating
@login_required
@permissmions_required("Owner", "Edit", "Comment", "View")
def show_task_detail(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    task_comments = task.task_comments.filter(parent_comment=None).order_by("-date")
    project = task.project
    user_membership_level = get_project_membership_level(request.user, project.id)

    context = {
        "user_membership_level": user_membership_level,
        "task": task,
        "task_comments": task_comments,
    }

    return render(request, "tasks/show_task_detail.html", context)


# Create Task Comment
@login_required
@permissmions_required("Owner", "Edit", "Comment")
def create_task_comment(request, task_id):
    task = get_object_or_404(Task, id=task_id)

    form = TaskCommentForm(request.POST) if request.method == "POST" else TaskCommentForm()

    if request.method == "POST" and form.is_valid():
        comment = form.save(False)
        comment.task = task
        comment.author = request.user
        comment.save()

        return redirect("show_task_detail", task_id=task_id)

    context = {
        "task": task,
        "form": form,
    }

    return render(request, "tasks/create_task_comment.html", context)


# Create Task Comment Reply
@login_required
@permissmions_required("Owner", "Edit", "Comment")
def create_task_comment_reply(request, task_id, task_comment_id):
    task = get_object_or_404(Task, id=task_id)
    parent_comment = get_object_or_404(TaskComment, id=task_comment_id)

    form = TaskCommentForm(request.POST) if request.method == "POST" else TaskCommentForm()

    if request.method == "POST" and form.is_valid():
        reply = form.save(False)
        reply.parent_comment = parent_comment
        reply.task = task
        reply.author = request.user
        reply.save()

        return redirect("show_task_detail", task_id=task.id)

    context = {
        "task": task,
        "parent_comment": parent_comment,
        "form": form,
    }

    return render(request, "tasks/create_task_comment_reply.html", context)
