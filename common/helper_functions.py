from django.shortcuts import get_object_or_404
from projects.models import Project

# Function to Determine User Project Membership Level
def get_project_membership_level(user, project_id):
    if project_id is None:
        return None
    
    project = get_object_or_404(Project, id=project_id)
    if user == project.owner:
        user_membership_level = "Owner"
    else:
        user_membership_object = project.members.filter(project=project, member=user).first()
        user_membership_level = user_membership_object.membership_level if user_membership_object else None

    return user_membership_level
