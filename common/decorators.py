# define outer decorator (convention is to use subject_verb or verb_subject (eg login_required))
# outer dec will take args for the perms required for the view func

    # define inner decorator
    # inner dec will take as its only arg, the function we want to decorate

        # define function processor, which takes *args, **kwargs (these args and kwargs collect the args we pass from the function we decorate)

            # do custom logic
            # if not custom logic:
                # redirect
            # else:
                # return decorated function


        # return function processor

    # return inner decorator

from django.shortcuts import render, redirect, get_object_or_404
from common.helper_functions import get_project_membership_level
from tasks.models import Task
# define outer decorator (convention is to use subject_verb or verb_subject (eg login_required))
# outer dec will take args for the perms required for the view func
def permissmions_required(*perms):

    # define inner decorator
    # inner dec will take as its only arg, the function we want to decorate
    def inner_decorator(decorated_function):

        # define function processor, which takes *args, **kwargs (these args and kwargs collect the args we pass from the function we decorate)
        def func_processor(*args, **kwargs):
            if kwargs.get("project_id"):
                user_membership_level = get_project_membership_level(args[0].user, kwargs.get("project_id"))
            elif kwargs.get("task_id"):
                task = Task.objects.get(id=kwargs.get("task_id"))
                project_id = task.project.id
                user_membership_level = get_project_membership_level(args[0].user, project_id)

            # do custom logic
            # if not custom logic:
            if user_membership_level not in perms:
                # redirect
                return redirect("login")
            # else:
            else:
                # return decorated function
                return decorated_function(*args, **kwargs)

        # return function processor
        return func_processor

    # return inner decorator
    return inner_decorator
