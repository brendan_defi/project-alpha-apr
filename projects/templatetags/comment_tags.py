from collections import namedtuple
from django import template
register = template.Library()
from projects.models import ProjectComment
from tasks.models import TaskComment


# Render Comment with Replies
@register.inclusion_tag("_single_comment.html")
def comment_with_replies(model, comment_id, user_membership_level):
    # Pull the comment object
    if model == "ProjectComment":
        comment = ProjectComment.objects.get(id=comment_id)
    elif model == "TaskComment":
        comment = TaskComment.objects.get(id=comment_id)
    # create a list that stores the comments and their depths
    current_comment = {"comment":comment, "depth": 0}
    # create a variable, comment_list, to ingest all of the comments and replies, initialized as a list with just the current_comment
    comment_list = [current_comment]

    # pull all of the replies to the original comment object,and append them to comment_inventory
    replies = comment.replies.all().order_by("date")
    # create a variable, reply_inventory, to process all of the comments and replies, initialized as the list of replies
    reply_inventory = [{"comment":reply, "depth": 1} for reply in replies]
    # iterate over reply_inventory using a stack data-structure
    while len(reply_inventory) > 0:
        # pop the first item out of reply_inventory, and store the contents as the current_comment
        current_comment = reply_inventory.pop(0)
        # and then append current_comment to the comment_list
        comment_list.append(current_comment)

        # then we need to pull any replies to current_comment
        current_comment_replies = current_comment["comment"].replies.all().order_by("-date")
        # pull the parent depth of the current comment, which we will increment for all child-comments
        current_comment_parent_depth = current_comment["depth"]
        # iterate over the replies to current_comment
        for comment in current_comment_replies:
            # add each reply to the front of reply_inventory
            reply_inventory = [{"comment":comment, "depth":current_comment_parent_depth + 1}] + reply_inventory

    # after the stack is emptied, put the info into context
    context = {
        "model": model,
        "comment_list": comment_list,
        "top_level_comment": comment_list[0],
        "reply_list": comment_list[1:],
        "user_membership_level": user_membership_level,
    }

    return context














# def comment_with_replies(project_comment_id):
#     # Pull the comment object
#     comment = ProjectComment.objects.get(id=project_comment_id)
#     # create a namedtuple that stores the comments and their depths
#     CommentDepth = namedtuple("CommentDepth", ["comment", "depth"])
#     # create a variable that creates tuples of each comment/reply, storing comments and their depths
#     comment_depth_tuple = CommentDepth(comment, 0)
#     # create a variable, comment_list, to ingest all of the comments and replies, initialized as a list with just the comment_object at dpeth 0
#     comment_list = [comment_depth_tuple]

#     # pull all of the replies to the original comment object,and append them to comment_inventory
#     replies = ProjectComment.objects.filter(parent_comment=comment).order_by("-date")
#     # create a variable, reply_inventory, to process all of the comments and replies, initialized as the list of replies
#     reply_inventory = list(replies)
#     # iterate over reply_inventory
#     for reply in reply_inventory:
#         # we first iterate over the comment_list to find the parent comment and its depth
#         parent_depth = 0
#         for comment_obj in reversed(comment_list):
#             if comment_obj[0] == reply.parent_comment:
#                 parent_depth = comment_obj[1]
#                 break
#         # then we create the comment_depth_tuple
#         comment_depth_tuple = CommentDepth(reply, parent_depth + 1)
#         # we then add each the comment_tuple to the comment_list
#         comment_list.append(comment_depth_tuple)
#         # for each reply, see if the reply is the parent_comment to any other comments
#         replies = ProjectComment.objects.filter(parent_comment=reply).order_by("-date")
#         # if there are further sub-replies, insert them to the start of reply_inventory
#         reply_inventory = list(replies) + reply_inventory

#     # once we have performed our iteration, we know we have exhausted all of the comments and replies
#     # so we return the comment_list

#     context = {
#         "comment_list": comment_list,

#         # "top_level_comment": comment_list[0],
#         # "reply_list": comment_list[1:],
#     }

#     return context
