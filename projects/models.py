from django.db import models
from django.conf import settings


# Project Model
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name


# Project Comment Model
class ProjectComment(models.Model):
    comment = models.TextField()
    date = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="project_comments",
        on_delete=models.PROTECT
    )
    project = models.ForeignKey(
        Project,
        related_name="project_comments",
        on_delete=models.CASCADE,
    )
    parent_comment = models.ForeignKey(
        "self",
        related_name="replies",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )

# Membership Level Choices
VIEW = "View"
COMMENT = "Comment"
EDIT = "Edit"

LEVEL_CHOICES = [
    (VIEW, "View"),
    (COMMENT, "Comment"),
    (EDIT, "Edit"),
]


# Project Membership Model
class ProjectMembership(models.Model):
    project = models.ForeignKey(
        Project,
        related_name="members",
        on_delete=models.CASCADE,
    )
    member = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="related_projects",
        on_delete=models.CASCADE,
    )
    membership_level = models.CharField(
        max_length=20,
        choices=LEVEL_CHOICES,
        default=VIEW,
    )
    updated_at = models.DateTimeField(auto_now_add=True)


# Project Membership Request Model
class ProjectMembershipRequest(models.Model):
    project = models.ForeignKey(
        Project,
        related_name="membership_requests",
        on_delete=models.CASCADE,
    )
    requester = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="project_membership_requests",
        on_delete=models.CASCADE,
    )
    request_level = models.CharField(
        max_length=20,
        choices=LEVEL_CHOICES,
        default=VIEW,
    )
    requested_at = models.DateTimeField(auto_now=True)
    is_approved = models.BooleanField(null=True, blank=True)
    is_expired = models.BooleanField(default=False)


# Project Membership Request Approval Model
class ProjectMembershipRequestApproval(models.Model):
    membership_request = models.ForeignKey(
        ProjectMembershipRequest,
        related_name="approvals",
        on_delete=models.CASCADE,
    )
    is_approved = models.BooleanField(null=True, blank=True)
    approval_level = models.CharField(
        max_length=20,
        choices=LEVEL_CHOICES,
        default=VIEW,
    )
    request_responder = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="project_membership_request_approvals",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None,
    )
    approved_at = models.DateTimeField(auto_now=True)


# New Classes Here
