from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from projects.models import Project, ProjectComment, ProjectMembership, ProjectMembershipRequest
from projects.forms import ProjectForm, ProjectCommentForm, ProjectMembershipForm, ProjectMembershipRequestForm
from common.decorators import permissmions_required
from common.helper_functions import get_project_membership_level
from tasks.models import Task


# Project List
@login_required
def browse_projects(request):
    project_list = Project.objects.all()
    # for project in project_list:
    #     if project.owner == request.user:
    #         project.user_membership_level = "Owner"
    #     elif request.user in project.members:
    #         project.user_membership_level = project.members.membership_level

    context = {
        "project_list": project_list,
    }

    return render(request, "projects/browse_projects.html", context)


# User's Project List
@login_required
def user_list_projects(request):
    owned_project_list = Project.objects.filter(owner=request.user)
    memberships_list = ProjectMembership.objects.filter(member=request.user)

    for project in owned_project_list:
        project.project_task_count = Task.objects.filter(project=project, is_completed=False).count()
        project.user_task_count = Task.objects.filter(project=project, assignee=request.user, is_completed=False).count()

    for membership in memberships_list:
        membership.project_task_count = Task.objects.filter(project=membership.project, is_completed=False).count()
        membership.user_task_count = Task.objects.filter(project=membership.project, assignee=request.user, is_completed=False).count()

    context = {
        "owned_project_list": owned_project_list,
        "memberships_list": memberships_list,
    }

    return render(request, "projects/user_list_projects.html", context)


# Project Detail w/ User Access Gating
def show_project(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    tasks = project.tasks.all().order_by("is_completed", "due_date")
    project_comments = project.project_comments.filter(parent_comment=None).order_by("-date")
    user_membership_level = get_project_membership_level(request.user, project.id)

    context = {
        "project": project,
        "user_membership_level": user_membership_level,
        "tasks": tasks if user_membership_level else None,
        "project_comments": project_comments if user_membership_level else None,
    }

    return render(request, "projects/show_project.html", context)


# Create a New Project
@login_required
def create_project(request):
    form = ProjectForm(request.POST or None)

    if request.method == "POST" and form.is_valid():
        form.save()
        return redirect("user_list_projects")

    context = {
        "form": form,
    }

    return render(request, "projects/create_project.html", context)


# Create a New Project Comment
@login_required
@permissmions_required("Owner", "Edit", "Comment")
def create_project_comment(request, project_id):
    project = get_object_or_404(Project, id=project_id)

    form = ProjectCommentForm(request.POST) if request.method == "POST" else ProjectCommentForm()

    if request.method == "POST" and form.is_valid():
        comment = form.save(False)
        comment.project = project
        comment.author = request.user
        comment.save()

        return redirect("show_project", project_id=project.id)

    context = {
        "project": project,
        "form": form,
    }

    return render(request, "projects/create_project_comment.html", context)


# Create a New Project Comment Reply
@login_required
@permissmions_required("Owner", "Edit", "Comment")
def create_project_comment_reply(request, project_id, project_comment_id):
    project = get_object_or_404(Project, id=project_id)
    parent_comment = get_object_or_404(ProjectComment, id=project_comment_id)

    form = ProjectCommentForm(request.POST) if request.method == "POST" else ProjectCommentForm()

    if request.method == "POST" and form.is_valid():
        reply = form.save(False)
        reply.parent_comment = parent_comment
        reply.project = project
        reply.author = request.user
        reply.save()

        return redirect("show_project", project_id=project.id)

    context = {
        "project": project,
        "parent_comment": parent_comment,
        "form": form,
    }

    return render(request, "projects/create_project_comment_reply.html", context)


# Request Project Membership
@login_required
def request_project_membership(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    form = ProjectMembershipRequestForm(request.POST) if request.method == "POST" else ProjectMembershipRequestForm()

    if request.method == "POST" and form.is_valid():
        membership_request = form.save(False)
        membership_request.requester = request.user
        membership_request.project = project
        existing_request = ProjectMembershipRequest.objects.filter(project=project, requester=request.user).last()

        if existing_request:
            existing_request.is_expired = True
            existing_request.save()

        membership_request.save()
        return redirect("home")

    context = {
        "project": project,
        "form": form,
    }

    return render(request, "projects/request_project_membership.html", context)


# Manage Project Membership
@login_required
@permissmions_required("Owner")
def manage_project_membership(request, project_id):
    if request.method == "GET":
        request.session["prev_page"] = request.META.get('HTTP_REFERER')

    project = get_object_or_404(Project, id=project_id)
    project_members = project.members.all()
    project_membership_requests = project.membership_requests.filter(is_approved=None, is_expired=False)
    new_member_form = ProjectMembershipForm(request.POST) if request.method == "POST" else ProjectMembershipForm()

    if request.method == "POST" and new_member_form.is_valid():
        new_approval = new_member_form.save(False)

        if not ProjectMembership.objects.filter(project=project, member=new_approval.member).exists():
            new_approval.project = project
            new_approval.save()

            existing_request = ProjectMembershipRequest.objects.filter(project=project, requester=new_approval.member, is_expired=False).first()

            if existing_request:
                existing_request.is_expired = True
                existing_request.save()

            return redirect(request.session.get('prev_page', 'home'))

        new_member_form.add_error(None, "This user is already a member of this project.")

    context = {
        "project": project,
        "project_members": project_members,
        "project_membership_requests": project_membership_requests,
        "new_member_form": new_member_form,
    }

    return render(request, "projects/manage_project_membership.html", context)


# Approve Membership Request
@login_required
@permissmions_required("Owner")
def approve_membership_request(request, project_id, membership_request_id):
    if request.method == "GET":
        request.session["prev_page"] = request.META.get('HTTP_REFERER')

    project = get_object_or_404(Project, id=project_id)
    membership_request = get_object_or_404(ProjectMembershipRequest, id=membership_request_id)
    form = ProjectMembershipRequestForm(request.POST, instance=membership_request) if request.method == "POST" \
        else ProjectMembershipRequestForm(instance=membership_request)

    if request.method == "POST" and form.is_valid():
        membership_approval = form.save(False)
        membership_approval.is_approved = True
        print(membership_approval)
        membership_approval.save()

        new_project_membership = ProjectMembership()
        new_project_membership.project = project
        new_project_membership.member = membership_approval.requester
        new_project_membership.membership_level = membership_approval.request_level
        new_project_membership.save()

        return redirect(request.session["prev_page"])

    context = {
        "project": project,
        "membership_request": membership_request,
        "form": form,
    }

    return render(request, "projects/approve_project_membership.html", context)


# Deny Membership Request
@login_required
@permissmions_required("Owner")
def deny_membership_request(request, project_id, membership_request_id):
    project = get_object_or_404(Project, id=project_id)
    membership_request = get_object_or_404(ProjectMembershipRequest, id=membership_request_id)

    membership_request.is_approved = False
    membership_request.approval_responder = request.user
    membership_request.save()

    return redirect("manage_project_membership", project_id=project.id)


# Update Project Membership
@login_required
@permissmions_required("Owner")
def update_project_membership(request, project_id, membership_id):
    if request.method == "GET":
        request.session["prev_page"] = request.META.get('HTTP_REFERER')

    project = get_object_or_404(Project, id=project_id)
    membership = get_object_or_404(ProjectMembership, id=membership_id)

    form = ProjectMembershipForm(request.POST, instance=membership) if request.method == "POST" \
        else ProjectMembershipForm(instance=membership)

    if request.method == "POST" and form.is_valid():
        # form = form.save(False)
        # form.member = membership.member
        # form.project = membership.project
        form.save()

        return redirect(request.session.get('prev_page', 'home'))

    context = {
        "project": project,
        "membership": membership,
        "form": form,
    }

    return render(request, "projects/update_project_membership.html", context)


# Remove Project Membership
@login_required
@permissmions_required("Owner")
def remove_project_membership(request, project_id, membership_id):
    project = get_object_or_404(Project, id=project_id)
    membership = get_object_or_404(ProjectMembership, id=membership_id)

    membership.delete()

    return redirect("manage_project_membership", project_id=project.id)
