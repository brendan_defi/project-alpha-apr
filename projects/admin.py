from django.contrib import admin
from projects.models import Project, ProjectComment, ProjectMembership, ProjectMembershipRequest


# Admin Model for Project
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "owner",
    ]


# Admin Model for ProjectComment
@admin.register(ProjectComment)
class ProjectCommentAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "project",
        "author",
    ]


# Admin Model for ProjectMembership
@admin.register(ProjectMembership)
class ProjectMembershipAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "member",
        "project",
        "membership_level",
    ]


# Admin Model for ProjectMembershipRequest
@admin.register(ProjectMembershipRequest)
class ProjectMembershipRequestAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "requester",
        "project",
        "request_level",
    ]
