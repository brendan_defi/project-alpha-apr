from django.forms import ModelForm
from projects.models import Project, ProjectComment, ProjectMembership, ProjectMembershipRequest


# Form to Create a Project
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]


# Form to Create a Project Comment
class ProjectCommentForm(ModelForm):
    class Meta:
        model = ProjectComment
        fields = [
            "comment",
        ]


# Form to Request Access to Project
class ProjectMembershipRequestForm(ModelForm):
    class Meta:
        model = ProjectMembershipRequest
        fields = [
            "request_level",
        ]


# Form to Approve User Acceess to Project
class ProjectMembershipForm(ModelForm):
    class Meta:
        model = ProjectMembership
        fields = [
            "member",
            "membership_level",
        ]
