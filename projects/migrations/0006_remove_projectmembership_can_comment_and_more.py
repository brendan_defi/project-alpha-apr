# Generated by Django 4.2.7 on 2023-11-14 23:41

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("projects", "0005_projectmembership"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="projectmembership",
            name="can_comment",
        ),
        migrations.RemoveField(
            model_name="projectmembership",
            name="can_edit",
        ),
        migrations.RemoveField(
            model_name="projectmembership",
            name="can_view",
        ),
        migrations.AddField(
            model_name="projectmembership",
            name="membership_level",
            field=models.CharField(
                choices=[
                    ("View", "View"),
                    ("Comment", "Comment"),
                    ("Edit", "Edit"),
                ],
                default="View",
                max_length=20,
            ),
        ),
        migrations.CreateModel(
            name="ProjectMembershipRequest",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("request_date", models.DateTimeField(auto_now=True)),
                (
                    "request_level",
                    models.CharField(
                        choices=[
                            ("View", "View"),
                            ("Comment", "Comment"),
                            ("Edit", "Edit"),
                        ],
                        default="View",
                        max_length=20,
                    ),
                ),
                (
                    "project",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="membership_requests",
                        to="projects.project",
                    ),
                ),
                (
                    "requester",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="project_membership_requests",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
    ]
