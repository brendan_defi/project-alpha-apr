# Generated by Django 4.2.7 on 2023-11-17 04:40

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        (
            "projects",
            "0010_rename_request_date_projectmembershiprequest_requested_at_and_more",
        ),
    ]

    operations = [
        migrations.RenameField(
            model_name="projectmembershiprequestapproval",
            old_name="approved_by",
            new_name="request_responder",
        ),
    ]
