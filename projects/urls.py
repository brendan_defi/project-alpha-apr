from django.urls import path
from projects.views import browse_projects, user_list_projects, show_project, create_project, \
    create_project_comment, create_project_comment_reply, request_project_membership, \
    manage_project_membership, approve_membership_request, deny_membership_request, \
    remove_project_membership, update_project_membership


urlpatterns = [
    path("create/", create_project, name="create_project"),
    path("all/", browse_projects, name="browse_projects"),
    path("mine/", user_list_projects, name="user_list_projects"),
    path("<int:project_id>", show_project, name="show_project"),
    path("<int:project_id>/request-access/", request_project_membership, name="request_project_membership"),
    path("<int:project_id>/manage-access/", manage_project_membership, name="manage_project_membership"),
    path("<int:project_id>/access-request/<int:membership_request_id>/approve/", approve_membership_request, name="approve_membership_request"),
    path("<int:project_id>/access-request/<int:membership_request_id>/deny/", deny_membership_request, name="deny_membership_request"),
    path("<int:project_id>/membership/<int:membership_id>/update/", update_project_membership, name="update_project_membership"),
    path("<int:project_id>/membership/<int:membership_id>/delete/", remove_project_membership, name="remove_project_membership"),
    path("<int:project_id>/comment/", create_project_comment, name="create_project_comment"),
    path("<int:project_id>/comment/<int:project_comment_id>/reply/", create_project_comment_reply, name="create_project_comment_reply"),
]
