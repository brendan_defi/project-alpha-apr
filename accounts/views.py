from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import LoginForm, SignupForm


# Login View
def user_login(request):
    # more gracefully handle the case where user is None
    form = LoginForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]

        user = authenticate(
            request,
            username=username,
            password=password,
        )

        if user is not None:
            login(request, user)
            return redirect("home")

    context = {
        "form": form,
    }

    return render(request, "accounts/login.html", context)
    ##############################################################
    # original code for login that handles `user is None` issues
    # refactored above to be DRY
    # if request.method == "POST":
    #     form = LoginForm(request.POST)
    #     if form.is_valid():
    #         username = form.cleaned_data["username"]
    #         password = form.cleaned_data["password"]

    #         user = authenticate(
    #             request,
    #             username=username,
    #             password=password,
    #         )

    #         if user is not None:
    #             login(request, user)
    #             return redirect("home")

    #         else:
    #             form = LoginForm()

    #             context = {
    #                 "form": form,
    #             }

    #             return render(request, "accounts/login.html", context)

    # else:
    #     form = LoginForm()

    #     context = {
    #         "form": form,
    #     }

    #     return render(request, "accounts/login.html", context)


# Logout View
def user_logout(request):
    logout(request)
    return redirect("login")


# Signup View
def user_signup(request):
    form = SignupForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        password_confirmation = form.cleaned_data["password_confirmation"]

        if not User.objects.filter(username=username).exists():
            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )

                login(request, user)
                return redirect("browse_projects")

            else:
                form.add_error(None, "the passwords do not match")

        else:
            form.add_error(
                None,
                "This username is already in use. Please choose another username.",
            )

    context = {
        "form": form,
    }

    return render(request, "accounts/signup.html", context)
